# UDF2 region
# ============
#
# Vertices of polygon to define region to use UDF2.

udf2raa  =  53.174052
udf2deca = -27.785841
udf2rab  =  53.161612
udf2decb = -27.775976
udf2rac  =  53.173478
udf2decc = -27.764314
udf2rad  =  53.185508
udf2decd = -27.774290

# The polygon option for ImageCrop used for these vertices.
udf2polygon = --polygon=$(udf2raa),$(udf2deca):$(udf2rab),$(udf2decb):$(udf2rac),$(udf2decc):$(udf2rad),$(udf2decd)
