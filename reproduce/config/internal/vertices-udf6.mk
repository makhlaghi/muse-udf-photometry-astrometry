# UDF6 region
# ============
#
# Vertices of polygon to define region to use UDF6.

udf6raa  =  53.175113
udf6deca = -27.808545
udf6rab  =  53.162673
udf6decb = -27.798725
udf6rac  =  53.173469
udf6decc = -27.787874
udf6rad  =  53.186128
udf6decd = -27.798305

# The polygon option for ImageCrop used for these vertices.
udf6polygon = --polygon=$(udf6raa),$(udf6deca):$(udf6rab),$(udf6decb):$(udf6rac),$(udf6decc):$(udf6rad),$(udf6decd)
