# UDF1 region
# ============
#
# Vertices of polygon to define region to use UDF1.

udf1raa  =  53.160532
udf1deca = -27.774896
udf1rab  =  53.147629
udf1decb = -27.764659
udf1rac  =  53.159066
udf1decc = -27.753664
udf1rad  =  53.171654
udf1decd = -27.763868

# The polygon option for ImageCrop used for these vertices.
udf1polygon = --polygon=$(udf1raa),$(udf1deca):$(udf1rab),$(udf1decb):$(udf1rac),$(udf1decc):$(udf1rad),$(udf1decd)
