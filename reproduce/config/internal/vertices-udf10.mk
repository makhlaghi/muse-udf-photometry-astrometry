# UDF10 region
# ============
#
# Vertices of polygon to define region to use UDF10.

udf10raa  =  53.170105
udf10deca = -27.786864
udf10rab  =  53.152458
udf10decb = -27.786585
udf10rac  =  53.152486
udf10decc = -27.770861
udf10rad  =  53.170294
udf10decd = -27.771096

# The polygon option for ImageCrop used for these vertices.
udf10polygon = --polygon=$(udf10raa),$(udf10deca):$(udf10rab),$(udf10decb):$(udf10rac),$(udf10decc):$(udf10rad),$(udf10decd)
