# UDF7 region
# ============
#
# Vertices of polygon to define region to use UDF7.

udf7raa  =  53.135275
udf7deca = -27.799024
udf7rab  =  53.122761
udf7decb = -27.788744
udf7rac  =  53.134394
udf7decc = -27.777903
udf7rad  =  53.146578
udf7decd = -27.787875

# The polygon option for ImageCrop used for these vertices.
udf7polygon = --polygon=$(udf7raa),$(udf7deca):$(udf7rab),$(udf7decb):$(udf7rac),$(udf7decc):$(udf7rad),$(udf7decd)
