# UDF3 region
# ============
#
# Vertices of polygon to define region to use UDF3.

udf3raa  =  53.187452
udf3deca = -27.797344
udf3rab  =  53.174848
udf3decb = -27.786957
udf3rac  =  53.186372
udf3decc = -27.775679
udf3rad  =  53.199274
udf3decd = -27.786258

# The polygon option for ImageCrop used for these vertices.
udf3polygon = --polygon=$(udf3raa),$(udf3deca):$(udf3rab),$(udf3decb):$(udf3rac),$(udf3decc):$(udf3rad),$(udf3decd)
