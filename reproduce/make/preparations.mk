# Top level preparations for all the other Makefiles.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# Make high-level directories
# ---------------------------
#
# If the value to BSYM (set in `config.mk') is not blank, then build a
# symbolic link to the build directory. Note that when the value is
# empty, it is not seen as a prerequisite to `all' either.
$(BDIR):; mkdir $@;
ifneq ($(BSYM),)
$(BSYM): | $(BDIR); ln -s $(BDIR) $(BSYM)
endif





# Top directories under BDIR and $(BDIR)/tex
# ------------------------------------------
#
# The outputs of the separate steps will be in the build directory,
# and all the TeX related output are stored in `$(BDIR)/tex'. We don't
# want to repeatedly call a command to build those directories, so we
# are defining a pattern rule to do the job for any similarly
# formatted directory.
#
# Just note that it is important that you do not put a `/' after the
# directory name when defining its variable, since in a pattern rule,
# an ending `/' will not match. Add the `/' everytime you want to call
# the directory variable.
mtexdir = $(BDIR)/tex/macros
$(BDIR)/%: | $(BDIR); mkdir -p $@





# Version information and TeX macros
# ----------------------------------
#
# Write all the necessary information into the TeX file. The versions
# need to be updated whenever the Git file has been updated (for
# example after a commit).
gitdescribe := $(shell git describe --dirty --always --abbrev=4)
gaversion := $(shell astmkprof --version | awk 'NR==1{print $$NF}')
$(mtexdir)/versions.tex: | $(mtexdir)
	@echo "\\newcommand{\\commit}{$(gitdescribe)}"        > $@
	@echo "\\newcommand{\\gnuastrover}{$(gaversion)}"    >> $@
