# Broad-band photometry checks with MUSE generated broad-band images.
#
# Prepare the input cutouts for further processing.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.





# Align the MUSE UDF region
# -------------------------
#
# The original MUSE UDF Mosaic region is not aligned with the
# celestial coordinates. To make things easier, we are first aligning
# them to ease the cutout process. This is done here (rather than
# `cutout-muse.mk') because we need the corrected scale factor
audfdir=$(BDIR)/aligned-udf
udfaligned = $(foreach f, $(filters), $(audfdir)/muse-udf-$(f).fits)
$(udfaligned): $(audfdir)/muse-udf-%.fits:                                  \
               $(MUSEINPUTS)/muse-udf-%.fits $(imgwarp) | $(audfdir)
	astimgwarp $< --align -h1 -o$@





# Cutout desired regions
# ----------------------
#
# Cutout each field's region from the aligned MUSE images.
mcutdir = $(BDIR)/muse-cutouts
udf1-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf1-$(f).fits)
$(udf1-muse-cutouts): $(mcutdir)/udf1-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf1.mk  \
	              | $(mcutdir)
	astimgcrop $(udf1polygon) $< -o$@

udf2-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf2-$(f).fits)
$(udf2-muse-cutouts): $(mcutdir)/udf2-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf2.mk  \
	              | $(mcutdir)
	astimgcrop $(udf2polygon) $< -o$@

udf3-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf3-$(f).fits)
$(udf3-muse-cutouts): $(mcutdir)/udf3-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf3.mk  \
	              | $(mcutdir)
	astimgcrop $(udf3polygon) $< -o$@

udf4-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf4-$(f).fits)
$(udf4-muse-cutouts): $(mcutdir)/udf4-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf4.mk  \
	              | $(mcutdir)
	astimgcrop $(udf4polygon) $< -o$@

udf5-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf5-$(f).fits)
$(udf5-muse-cutouts): $(mcutdir)/udf5-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf5.mk  \
	              | $(mcutdir)
	astimgcrop $(udf5polygon) $< -o$@

udf6-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf6-$(f).fits)
$(udf6-muse-cutouts): $(mcutdir)/udf6-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf6.mk  \
	              | $(mcutdir)
	astimgcrop $(udf6polygon) $< -o$@

udf7-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf7-$(f).fits)
$(udf7-muse-cutouts): $(mcutdir)/udf7-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf7.mk  \
	              | $(mcutdir)
	astimgcrop $(udf7polygon) $< -o$@

udf8-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf8-$(f).fits)
$(udf8-muse-cutouts): $(mcutdir)/udf8-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf8.mk  \
	              | $(mcutdir)
	astimgcrop $(udf8polygon) $< -o$@

udf9-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf9-$(f).fits)
$(udf9-muse-cutouts): $(mcutdir)/udf9-%.fits: $(audfdir)/muse-udf-%.fits     \
	              $(imgcrop) reproduce/config/internal/vertices-udf9.mk  \
	              | $(mcutdir)
	astimgcrop $(udf9polygon) $< -o$@

udf10-muse-cutouts = $(foreach f, $(filters), $(mcutdir)/udf10-$(f).fits)
$(udf10-muse-cutouts): $(mcutdir)/udf10-%.fits:                              \
                       $(MUSEINPUTS)/muse-udf10-%.fits $(imgcrop)            \
                       reproduce/config/internal/vertices-udf10.mk | $(mcutdir)
	astimgcrop $(udf10polygon) -h1 $< -o$@





# Correct MUSE image size
# -----------------------
#
# To generate the catalog, all the inputs have to have the same
# size. However, due to differences in the WCS, after being warped,
# some fields might give a different size (by one pixel) compared to
# the MUSE cropped image.
cutdir = $(BDIR)/cutouts
muse-corr = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                         \
	       $(foreach f, $(filters), $(cutdir)/udf$(uid)-$(f)-m.fits) )
$(muse-corr): $(cutdir)/%-m.fits: $(mcutdir)/%.fits $(cutdir)/%-h.fits

        # If the sizes are identical, then just copy the actual
        # cropped MUSE image, otherwise, using ImageCrop's `--section'
        # option, specify if a road should be added or removed from
        # the MUSE image.
	msize=$$(astheader $(mcutdir)/$*.fits  | awk '$$1=="NAXIS1" {x=$$3}  \
	            $$1=="NAXIS2"{y=$$3} END{print x, y}');                  \
	hsize=$$(astheader $(cutdir)/$*-h.fits | awk '$$1=="NAXIS1" {x=$$3}  \
	            $$1=="NAXIS2"{y=$$3} END{print x, y}');                  \
	if [ "$$msize" = "$$hsize" ]; then                                   \
	  cp $< $@;                                                          \
	else                                                                 \
	  so=$$(echo "$$msize $$hsize"                                       \
	             | awk '{x = $$1==$$3 ? ":" : sprintf("%d:", 1+$$1-$$3); \
	                     y = $$2==$$4 ? ":" : sprintf("%d:", 1+$$2-$$4); \
	                     printf("--section=%s,%s", x, y)}');             \
	  astimgcrop $< $$so -o$@;                                           \
	fi





# NoiseChisel on MUSE images for astrometry comparison
# ----------------------------------------------------
#
# For magnitude comparison we used the NoiseChisel runs on HST images,
# since fainter objects can be detected there. But for astrometry, it
# is important to use the MUSE images for detection, since the result
# on a region with no MUSE signal will largely be determined by the
# aperture set with HST. So here, we will run NoiseChisl on the MUSE
# images and make a segmentation map to generate a catalog.
#
# The process is almost identical with the HST NoiseChisel run, look
# there for comments.
msegdir = $(BDIR)/seg-muse
seg-muse = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                          \
	      $(foreach f, $(filters), $(msegdir)/udf$(uid)-$(f).fits) )
$(seg-muse): $(msegdir)/%.fits: $(cutdir)/%-m.fits $(onepkernel)         \
	     $(noisechisel) | $(msegdir)

	astnoisechisel $< -o$(@D)/$*-nc.fits --kernel=$(onepkernel)      \
	               --minbfrac=0.0 --minmodeq=0.3 --qthresh=0.40      \
	               --dthresh=0.8 --detsnminarea=5 --minnumfalse=30   \
	               --segsnminarea=5 --segquant=0.5 --gthresh=1e10    \
	               --objbordersn=1e10

	astarithmetic $(@D)/$*-nc.fits $(@D)/$*-nc.fits 1 neq 0 where    \
	              -h1 -h2 -o$@ --type=long
